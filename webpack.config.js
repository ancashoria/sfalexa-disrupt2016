var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var config = {
    entry: './client/index.js',
    output: {
        path: './public',
        filename: 'bundle.js'
    },

    devtool: 'eval',

    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules)/,
                loader: 'babel',
                query: {
                    presets: ['es2015', 'stage-2', 'react']
                }
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract(['css', 'sass'])
            }
        ]
    },

    sassLoader: {
        includePaths: [ './client' ]
    },

    plugins: [
        new ExtractTextPlugin('bundle.css')
    ],

    resolve: {
        extensions: ['', '.js', '.jsx'],
        root: path.resolve('./client')
    },

    stats: {
        children: false
    }
};

module.exports = config;