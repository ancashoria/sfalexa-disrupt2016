var express = require('express');
var exphbs  = require('express-handlebars');
var path = require('path');

console.log('Starting the server..');

var app = express();
var port = 3000;

app.use(express.static(path.join(__dirname, 'public')));


// Redirect http to https
app.use(function (req, res, next) {
    if (req.headers['x-forwarded-proto'] !== 'https' && req.get('Host') !== 'localhost:3000') {
        return res.redirect(['https://', req.get('Host'), req.url].join(''));
    }
    return next();
});

// Set handlebars as view engine
var hbs = exphbs.create({ extname: '.hbs' });
app.engine('.hbs', hbs.engine);
app.set('views', path.join(__dirname, 'server/views'));
app.set('view engine', '.hbs');

app.use('/api/speech-to-text', require('./server/routes/stt-token'));
app.use('/api/text-to-speech', require('./server/routes/tts-token'));
app.use('/', require('./server/routes/index'));

app.listen(process.env.PORT || port, function () {
    console.log('Server running on port ' + port);
    console.log('See http://localhost:' + port + '/');
});