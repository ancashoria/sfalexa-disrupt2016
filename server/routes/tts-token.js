var express = require('express'),
    router = express.Router(), // eslint-disable-line new-cap
    vcapServices = require('vcap_services'),
    extend = require('util')._extend,
    config = require('../../config'),
    watson = require('watson-developer-cloud');

var ttsConfig = extend(config.text_to_speech, vcapServices.getCredentials('text_to_speech'));

var ttsAuthService = watson.authorization(ttsConfig);

router.get('/token', function(req, res) {
    ttsAuthService.getToken({url: ttsConfig.url}, function(err, token) {
        if (err) {
            console.log('Error retrieving token: ', err);
            res.status(500).send('Error retrieving token');
            return;
        }
        res.send(token);
    });
});

module.exports = router;