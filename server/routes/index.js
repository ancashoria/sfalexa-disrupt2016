const express = require('express');
const router = express.Router();
const request = require('request');
const config = require('../../config').tone_analyzer;

router.get('/api/analyze', (req, res) => {
    const options = {
        'auth': {
            'user': config.username,
            'pass': config.password
        }
    };
    request(`${config.url}/v3/tone?version=2016-05-19&text=${req.query.text}`, options, (error, response, body) => {
        if (!error && response.statusCode === 200) {
            return res.send(body);
        }
        res.send('Error');
    });
});

router.get('*', (req, res) => {
    res.render('index.hbs', {
        analytics: req.get('Host').indexOf('emotionjournal.online') !== -1
    });
});


module.exports = router;