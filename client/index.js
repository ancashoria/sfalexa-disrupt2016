import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, browserHistory} from 'react-router';

import Home from './pages/home';
import Chat from './pages/chat';
import Analyze from './pages/analyze'
import './base.scss';

ReactDOM.render((
    <Router history={browserHistory}>
        <Route path="/" component={Home}/>
        <Route path="chat" component={Chat}/>
        <Route path="analyze" component={Analyze}/>
    </Router>
), document.getElementById('app'));

console.log(
`███████╗███████╗     █████╗ ██████╗ ██████╗ ██╗    ██╗ ██████╗ ██████╗ ██╗  ██╗███████╗
██╔════╝██╔════╝    ██╔══██╗██╔══██╗██╔══██╗██║    ██║██╔═══██╗██╔══██╗██║ ██╔╝██╔════╝
███████╗█████╗      ███████║██████╔╝██████╔╝██║ █╗ ██║██║   ██║██████╔╝█████╔╝ ███████╗
╚════██║██╔══╝      ██╔══██║██╔═══╝ ██╔═══╝ ██║███╗██║██║   ██║██╔══██╗██╔═██╗ ╚════██║
███████║██║         ██║  ██║██║     ██║     ╚███╔███╔╝╚██████╔╝██║  ██║██║  ██╗███████║
╚══════╝╚═╝         ╚═╝  ╚═╝╚═╝     ╚═╝      ╚══╝╚══╝  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝
                                                                                       
                                                                                       
                                                                                       
`);