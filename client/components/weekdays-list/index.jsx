import React from 'react';

import './style.scss';

class WeekdaysList extends React.Component {

    render() {
        const {list} = this.props;

        return (
            <div className="weekdays-list">
                <ul>
                    <li className="calendar"><i className="fa fa-calendar" aria-hidden="true"/></li>
                    {
                        list.map((item, index) => {
                            return (
                                <li className={`${item.class} ${item.active ? 'active' : ''}`} key={ index }>
                                    <span className="day-name">{ item.dayName }</span>
                                    { !item.active ? <span className="date">{ item.date }</span> : null }
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        )
    }
}


export default WeekdaysList;