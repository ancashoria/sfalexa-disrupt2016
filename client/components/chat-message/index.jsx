import React from 'react';

import './style.scss';

export default ({ message, hidden }) => (
    <div className={ 'chat-message' + (hidden ? ' chat-message--hidden' : '') }>
        { message }
    </div>
);