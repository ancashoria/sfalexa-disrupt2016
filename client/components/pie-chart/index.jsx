import React from 'react';
import './style.scss';

class PieChart extends React.Component {

    componentDidMount() {
        const {text, dataPoints} = this.props;
        const chart = new CanvasJS.Chart("chartContainer",
            {
                theme: "theme2",
                title: {text},
                animationEnabled: true,
                legend: {
                    verticalAlign: "center",
                    horizontalAlign: "left",
                    fontSize: 20,
                    fontFamily: "Helvetica"
                },
                data: [
                    {
                        type: "pie",
                        indexLabelFontFamily: "Open Sans",
                        indexLabelFontSize: 20,
                        indexLabelFontWeight: "300",
                        startAngle: 0,
                        indexLabelFontColor: "#fff",
                        indexLabelLineColor: "#222",
                        indexLabelPlacement: "inside",
                        toolTipContent: "{name}: {y}hrs",
                        showInLegend: true,
                        indexLabel: "#percent%",
                        dataPoints
                    }
                ]
            });
        chart.render();
    }

    render() {
        return (
            <div id="chartContainer"></div>
        )
    }
}


export default PieChart;