import React from 'react';
import moment from 'moment';

import './style.scss';

export default () => (
    <div className="current-date">
        { moment().format("dddd, MMMM Do, YYYY") }
    </div>
);