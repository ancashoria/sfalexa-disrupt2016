import React from 'react';
import './style.scss';

class DescriptionList extends React.Component {

    render() {
        const {list} = this.props;

        return (
            <div className="description-list">
                <ul>
                    {
                        list.map((item, index) => {
                            return ( <li className={ item.class } key={ index }>{ item.value }</li> )
                        })
                    }
                </ul>
            </div>
        )
    }
}


export default DescriptionList;