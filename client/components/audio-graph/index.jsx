import React from 'react';
import random from 'lodash/random';

import './style.scss';

class AudioGraph extends React.Component {
    constructor(props) {
        super(props);
    
        this.state = {
            heights: new Array(props.items).fill(10)
        };

        this.changeHeights = this.changeHeights.bind(this);
        if (props.animate) {
            this.startAnimation();
        }
    }

    getMaxHeight() {
        return (this.props.maxVolume / 100) * 200;
    }

    startAnimation() {
        this.animationInterval = setInterval(this.changeHeights, 200);
    }

    stopAnimation() {
        this.setState({
            heights: new Array(this.props.items).fill(10)
        });

        clearInterval(this.animationInterval);
        this.animationInterval = null;
    }

    componentDidUpdate(prevProps) {
        if (prevProps.animate && !this.props.animate) {
            this.stopAnimation();
        } else if (!prevProps.animate && this.props.animate) {
            this.startAnimation();
        }
    }

    componentWillUnmount() {
        this.stopAnimation();
    }

    generateBump(length) {
        const curveHeight = random(15, this.getMaxHeight());
        const minHeight = 5;
        const stepSize = (curveHeight - minHeight) / length;
        const heights = [ 5 ];
        const middle = Math.floor(length / 2);

        for (let i=1; i<length; i++) {
            if (i <= middle) {
                heights.push(heights[i-1] + stepSize);
            } else {
                heights.push(heights[i-1] - stepSize);
            }
        }

        return heights;
    }

    changeHeights() {
        let heights = [];
        let currentPosition = 0;
        const slices = [];

        while (currentPosition < this.props.items) {
            const slice = random(5, 15);
            if (this.props.items - (currentPosition + slice) < 5) {
                slices.push(this.props.items - currentPosition);
                currentPosition = this.props.items;
            } else {
                currentPosition += slice;
                slices.push(slice);
            }
        }

        for (let i=0; i<slices.length; i++) {
            heights = [ ...heights, ...this.generateBump(slices[i]) ];
        }

        this.setState({ heights });
    }

    render() {
        const heights = this.state.heights;

        return (
            <div className="audio-graph">
                { heights.map((height, i) => (
                    <div className="audio-graph__item" style={{ height: `${height}px` }} key={ i }></div>
                )) }
            </div>
        );
    }
}

export default AudioGraph;