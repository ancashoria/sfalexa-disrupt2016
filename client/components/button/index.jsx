import React from 'react';
import './style.scss';

class Button extends React.Component {

    render() {
        const { text, onClickHandler } = this.props;

        return (
            <button onClick={ () => onClickHandler() } className="main-button">{ text }</button>
        )
    }
}


export default Button;