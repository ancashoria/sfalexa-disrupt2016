import React from 'react';
import debounce from 'lodash/debounce';

class SpeechToText extends React.Component {

    constructor(props) {
        super(props);

        this.stream = null;
        this.watch = null;

        this.watchChanges = this.watchChanges.bind(this);

        this.state = {
            text: '',
            lastPhrase: '',
            lastDot: -1,
            talking: false,
            longIdle: false
        };

        this.checkForIdle = debounce(() => {
            this.setState({ talking: false });
        }, 400, { leading: false });

        this.checkForLongIdle = debounce(() => {
            this.setState({ longIdle: true });
        }, 1500, { leading: false });
    }

    componentDidMount() {
        this.start();
    }

    componentWillUnmount() {
        this.stream.stop();
        this.stream = null;

        clearInterval(this.watch);
        this.watch = null;
    }

    componentWillUpdate(nextProps, nextState) {
        if (!this.state.talking && nextState.talking) {
            this.props.onStartTalking && this.props.onStartTalking();
        }

        if (this.state.talking && !nextState.talking) {
            this.props.onIdle && this.props.onIdle();
        }

        if (!this.state.longIdle && nextState.longIdle) {
            this.props.onLongIdle && this.props.onLongIdle();
        }
    }

    start() {
        fetch('/api/speech-to-text/token')
            .then((response) => {
                return response.text();
            }).then((token) => {
                this.stream = WatsonSpeech.SpeechToText.recognizeMicrophone({
                    token: token,
                    outputElement: '#speech-to-text-output'
                });

                this.watch = setInterval(this.watchChanges, 300);
            });
    }

    watchChanges() {
        if (this.refs.output.value !== this.state.text) {
            this.setState({
                text: this.refs.output.value,
                talking: true,
                longIdle: false
            }, this.newPhraseDetected);
            this.checkForIdle();
            this.checkForLongIdle();
        }
    }

    newPhraseDetected() {
        const lastPhrase = this.getLastPhrase(this.state.text);
        this.props.onNewPhrase && this.props.onNewPhrase(lastPhrase);
        this.setState({lastPhrase});
    }

    getLastPhrase(str) {
        const lastDot = str.lastIndexOf('.');
        if (lastDot > -1) {
            this.setState({lastDot});
            return str.substr(this.state.lastDot + 1, lastDot + 1).trim();
        }

        return str;
    }

    render() {
        return (
            <div style={{ display: 'none' }}>
                <textarea id='speech-to-text-output' ref='output'/>
            </div>
        );
    }
}

export default SpeechToText;