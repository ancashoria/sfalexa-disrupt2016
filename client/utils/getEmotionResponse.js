// Get a response message based on an emotion
import random from 'lodash/random';

const MESSAGES = {
    anger: [
        `I'm sorry, tell me more`,
    ],
    fear: [
        `I'm sorry, tell me more`,
    ],
    disgust: [
        `I'm sorry, tell me more`,
    ], 
    sadness: [
        `I'm sorry, tell me more`,
    ], 
    joy: [
        `Wow, that sound's awesome!`,
        `Cool, that sounds good!`,
        `Oh nice, I'm glad!`
    ]
};

export default (emotion) => {
    const messages = MESSAGES[emotion];

    return messages[random(0, messages.length - 1)];
};