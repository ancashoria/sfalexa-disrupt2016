const storage = {};

export default {
    set: (key, value) => {
        storage[key] = value
    },

    get: (key) => {
        return storage[key]
    }
}