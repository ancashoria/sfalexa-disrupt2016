export default (text, onEnd = () => {}) => {
    fetch('/api/text-to-speech/token')
        .then((response) => {
            return response.text();
        }).then((token) => {
            const tts = WatsonSpeech.TextToSpeech.synthesize({
                text: text,
                token: token,
                voice: 'en-US_AllisonVoice'
            });

            tts.addEventListener('ended', onEnd);
        });
}