import React from 'react';

import PieChart from 'components/pie-chart';
import DescriptionList from 'components/description-list';
import CurrentDate from 'components/current-date';
import WeekdaysList from 'components/weekdays-list';
import Storage from 'utils/store';
import './style.scss';
import {colors} from '../../../config';
import _ from 'lodash';
import range from 'lodash/range';
import moment from 'moment';

class Analyze extends React.Component {

    constructor() {
        super();
        this.state = Analyze.constructState(Storage.get('phrases'));
    }

    static constructState(phrases = []) {
        const state = {list: [], dataPoints: [], primaryFeeling: null, phrases};
        const emotionMap = {anger: [], fear: [], disgust: [], sadness: [], joy: []};
        let total = 0;
        let biggest = 0;

        // Create the list and the emotions map
        phrases.forEach((phrase) => {
            if (emotionMap[phrase.emotion.tone_id].length < 2) {
                state.list.push({class: phrase.predominantEmotion.tone_id, value: phrase.text});
                emotionMap[phrase.emotion.tone_id].push(phrase.emotion.score);
            }
        });


        // Calculate total
        Object.keys(emotionMap).forEach((key) => {
            total += _.sum(emotionMap[key]);
        });

        // Construct the graph data points
        Object.keys(emotionMap).forEach((key) => {
            const percentage = _.sum(emotionMap[key]) / total * 100 || 0;

            if (percentage > 0) {
                state.dataPoints.push({
                    color: colors[key],
                    legendText: _.upperFirst(key),
                    y: Math.round(percentage),
                    label: _.upperFirst(key)
                });
            }

            if (percentage > biggest) {
                biggest = percentage;
                state.primaryFeeling = key;
            }
        });

        return state;
    }

    render() {
        const phrases = this.state.phrases;
        const emotions = [ 'sadness', 'anger', 'anger', 'joy', 'fear', 'joy', this.state.primaryFeeling ];
        const weekdaysList = range(5, -1, -1).map((index, i) => {
            const date = moment().subtract(index, 'day');
            return {
                class: emotions[i],
                dayName: index !== 0 ? date.format('dddd') : 'Today',
                date: date.format('MMM Do'),
                active: index === 0
            };
        });

        const firstEmotion = phrases[0].predominantEmotion;
        const lastEmotion = phrases[phrases.length - 1].predominantEmotion;
        const started = firstEmotion.tone_id === 'joy' ? 'Good Day' : 'Bad Day';
        const ended = lastEmotion.tone_id === 'joy' ? 'Good Day' : 'Bad Day';
        const overall = this.state.primaryFeeling === 'joy' ? 'Good Day' : 'Bad Day';

        return (
            <div className="analyze-page">
                <div className="chart-area">
                    <CurrentDate />
                    <img className="logo" src="TheEmotionJournal_Logo_Dark.png" />

                    {
                        started !== ended ?
                            <p className='title'>
                                It started as a <strong className={firstEmotion.tone_id}>{started}</strong>,
                                but ended as a <strong className={lastEmotion.tone_id}>{ended}</strong>
                            </p>
                            :
                            <p className='title'>
                                Today was a <strong className={this.state.primaryFeeling}>{overall}</strong>
                            </p>
                    }

                    <PieChart dataPoints={ this.state.dataPoints }/>
                    <DescriptionList list={ _.reverse(this.state.list) }/>
                </div>
                <WeekdaysList list={ weekdaysList }/>
            </div>
        );
    }
}

export default Analyze;