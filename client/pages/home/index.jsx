import React from 'react';
import AudioGraph from 'components/audio-graph';
import {Link} from 'react-router';

import CurrentDate from 'components/current-date';
import './style.scss';

class Home extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div className="home-page">
                <CurrentDate />
                <img className="logo" src="TheEmotionJournal_Logo_Light.png" />
                <Link className="home-page__cta" to='chat'>
                    Let's get started
                    <span className="home-page__cta-background"></span>
                </Link>
                <AudioGraph items={ 50 } animate={ true } maxVolume={ 30 } />
            </div>
        )
    }
}


export default Home;