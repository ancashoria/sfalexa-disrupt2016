import React from 'react';
import classNames from 'classnames';
import {Link, browserHistory} from 'react-router';
import debounce from 'lodash/debounce';

import CurrentDate from 'components/current-date';
import AudioGraph from 'components/audio-graph';
import SpeechToText from 'components/speech-to-text';
import ChatMessage from 'components/chat-message';
import Storage from 'utils/store';
import textToSpeech from 'utils/text-to-speech';
import getEmotionResponse from 'utils/getEmotionResponse';
import './style.scss';

const ENDING_PHRASES = [
    'The end.', 'The ends.', 'That\'s all for today.', 'That\'s all for the day.',
    'That is all for today.', 'That is all for the day.', 'Well that\'s all for today',
    'Well that\'s all for the day', 'Well that is all for today', 'Well that is all for the day'
]

class Chat extends React.Component {
    constructor() {
        super();

        this.onNewPhrase = this.onNewPhrase.bind(this);
        this.onStartTalking = this.onStartTalking.bind(this);
        this.onIdle = this.onIdle.bind(this);
        this.onLongIdle = this.onLongIdle.bind(this);

        this.state = {
            speaking: true,
            botSpeaking: false,
            fadeOut: false,
            emotion: {tone_id: 'disgust', tone_name: ''},
            previousPhrase: null,
            currentPhrase: null,
            maxVolume: 40,
            respondedOnLongIdle: false
        };

        this.processEmotions = debounce(this.processEmotions.bind(this), 500);

        setTimeout(() => this.pushBotMessage('How was your day?'), 100);
    }

    pushBotMessage(message) {
        textToSpeech(message, () => this.setState({maxVolume: 40, botSpeaking: false}));
        setTimeout(() => this.setState({ maxVolume: 150 }), 1000);

        this.setState({
            previousPhrase: this.state.currentPhrase,
            currentPhrase: message,
            botSpeaking: true
        });
    }

    onStartTalking() {
        this.setState({
            maxVolume: 180
        });
    }

    onIdle() {
        this.setState({
            maxVolume: 40
        });
    }

    onLongIdle() {
        if (!this.state.respondedOnLongIdle) {
            const phrases = Storage.get('phrases');
            if (phrases && phrases.length) {
                this.setState({
                    respondedOnLongIdle: true
                });

                const lastPhrase = phrases[phrases.length - 1];
                this.pushBotMessage(getEmotionResponse(lastPhrase.predominantEmotion.tone_id));
            }
        }
    }

    /** Analyze all emotions from a text */
    processEmotions(text) {
        return fetch(`/api/analyze?text=${text}`)
            .then((res) => res.json())
            .then((json) => {
                const TONE_CATEGORIES = {EMOTION: 0, LANGUAGE: 1, SOCIAL: 2};
                const emotions = json.document_tone.tone_categories[TONE_CATEGORIES.EMOTION].tones;
                let predominantEmotion = {score: 0};

                // Find the predominant emotion and set it
                emotions.forEach((emotion) => {
                    if (emotion.score > predominantEmotion.score && emotion.tone_id) {
                        predominantEmotion = emotion;
                    }
                });

                if (predominantEmotion.tone_id) {
                    this.setState({emotion: predominantEmotion});
                } else {
                    predominantEmotion = null;
                }

                return {emotion: predominantEmotion, predominantEmotion, text};
            })
            .then((phrase) => this.isSentence(phrase.text) && phrase.predominantEmotion && this.persistPhrase(phrase));
    }

    persistPhrase(phrase) {
        const phrases = Storage.get('phrases') || [];
        Storage.set('phrases', [...phrases, phrase]);
    }

    isSentence(phrase) {
        return phrase.substr(-1) === '.';
    }

    onNewPhrase(phrase) {
        if (this.isSentence(phrase)) {
            this.setState({
                previousPhrase: this.state.currentPhrase,
                currentPhrase: phrase
            });

            if (ENDING_PHRASES.indexOf(phrase) !== -1) {
                setTimeout(() => {
                    console.log(Storage.get('phrases'));
                    this.setState('fadeOut', true);
                }, 1500);

                setTimeout(() => {
                    browserHistory.push('/analyze');
                }, 2000);
            } else {
                this.processEmotions(phrase);
            }
        }
    }

    handleClick() {
        if (Storage.get('phrases')) {
            browserHistory.push('/analyze');
        }
    }

    render() {
        const emotion = this.state.emotion;
        const mainClass = classNames('chat', emotion.tone_id, {
            fadeOut: this.state.fadeOut
        });
        return (
            <div className={mainClass} onClick={this.handleClick}>
                <img className="logo" src="TheEmotionJournal_Logo_Dark.png" />
                <CurrentDate />

                { emotion.tone_name ? <h3 className="emotion-name">{`${emotion.tone_name}`}</h3> : ''}

                { this.state.previousPhrase ?
                    <ChatMessage key={ this.state.previousPhrase } message={ this.state.previousPhrase }
                                 hidden={ true }/> : null }
                { this.state.currentPhrase ?
                    <ChatMessage key={ this.state.currentPhrase } message={ this.state.currentPhrase }/> : null }

                <AudioGraph items={ 50 } animate={ this.state.speaking } maxVolume={ this.state.maxVolume }/>
                { !this.state.botSpeaking ? 
                    <SpeechToText onNewPhrase={ this.onNewPhrase } onStartTalking={ this.onStartTalking }
                                  onIdle={ this.onIdle } onLongIdle={ this.onLongIdle }/> : null }
            </div>
        );
    }
}

export default Chat;